//---------------------------------------------------------------------------

#ifndef TModelListH
#define TModelListH
#include "fstream.h"
#include "TContainers.h"
struct TRegularModel {
	int len;
	int *model;

};

struct TTableInfo {
	int len;
	int *catSize;
};

struct TUnregularModel {
	int len;
	int *model;
	int count;
	int **var;
};

struct TDetailedModel {
	int len;
	int *model;
	int count;
	int **var;
	TDetailedModel(){};
	~TDetailedModel() {
		for(int i=0; i<count; i++)
			delete [] var[i];
		delete [] var;
		delete [] model;
	};
};

struct TExDetailedModel {
	int *model;
	bool *m;
	TDetailedModel *dm;
	TExDetailedModel(TDetailedModel* pdm) {
		dm = pdm; pdm = 0;
		model = new int[dm->len];
		m = new bool[dm->len];
	}

	int operator()(int i, int j) {
		//if(!m[j]) return dm->model[i];
		//else return model[i];
		return m[j]?model[i]:dm->model[i];
	};

	~TExDetailedModel() {
		delete dm;
		delete [] model;
		delete [] m;
    }
};


class TModelList {
	//private:
	public:
		TRegularModel **nounReg;
		int nounRegCount;
		TUnregularModel **nounUnreg;
		int nounUnregCount;
		TRegularModel **nounRegEx;
		int nounRegExCount;
		TUnregularModel **nounUnregEx;
		int nounUnregExCount;

		TRegularModel **verbReg;
		int verbRegCount;
		TUnregularModel **verbUnreg;
		int verbUnregCount;
		TRegularModel **verbRegEx;
		int verbRegExCount;
		TUnregularModel **verbUnregEx;
		int verbUnregExCount;

		TTableInfo tableInfo;

		static TRegularModel *CreateModel(int mlen, int *mod);
		static void DestroyModel(TRegularModel *model);
		static TUnregularModel *CreateModel(int mlen, int *mod, int vcount, int **var);
		static void DestroyModel(TUnregularModel *model);

		bool Load(char *filename, TRegularModel **modelList, int &count);
		bool Load(char *filename, TUnregularModel **modelList, int &count);
		TExDetailedModel* RepairModel(TDetailedModel *dm);
	public:
		TModelList();
		~TModelList();
		int LoadModels(char *nounReg,
					   char *nounUnreg,
					   char *nounRegEx,
					   char *nounUnregEx,
					   char *verbReg,
					   char *verbUnreg,
					   char *verbRegEx,
					   char *verbUnregEx);
		void LoadTableInfo(TTableInfo *info);

		TDetailedModel* CreateDetailed(TRegularModel *model);
		TDetailedModel* CreateDetailed(TUnregularModel *model);

		bool CheckFrame(TWordFrame &frame);

};


//---------------------------------------------------------------------------
#endif
