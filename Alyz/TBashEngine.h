//---------------------------------------------------------------------------
#include "TAffixTable.h"
#include "TRootBase.h"
#include "TModelList.h"
#include "TAnalyzTree.h"

#ifndef TBashEngineH
#define TBashEngineH




class TBashAnalyzer {
	public:
		static TAffixTable AffixTable;
		static TRootBase RootBase;
		static TModelList ModelList;



	public:
		void Init();
		void AnalyzeWord(wchar_t const*str, vector<TWordFrame> &result);
		void AnalyzeWord2(wchar_t const*str, vector<TWordFrame> &result);

};

struct TFrameList {
	TWordFrame frame[4];
	int curr;
	void AddAff(TAffixInfo& aff) {
		frame[curr].Add(aff);
	};
	void New(TAffixInfo aff);
};

//---------------------------------------------------------------------------
#endif
