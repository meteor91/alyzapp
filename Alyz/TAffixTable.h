//---------------------------------------------------------------------------

#include "TModelList.h"
//#include "TRootBase.h"
#include "TContainers.h"
#include <wchar.h>
#ifndef TAffixTableH
#define TAffixTableH

class TAffixTable;


struct TAffix {
	wchar_t *str;
	int p, t, len;
};

struct TAffixBlock {
	TAffix *block[4][4];
};

class TCategory {
	friend class TAffixTable;
	int count;
	TAffixBlock **block;
	int bad;
};


class TAffixTable {
	friend class TSearchIterator;
	public:
		TAffixBlock ***table;
		int mCount;
		int *mode;
		int mPossibleRes[15][11];


		static TAffix* CreateAffix(wchar_t *pstr, int len);
		static TAffixBlock* CreateBlock();
		static TCategory *CreateCategory();
		static void DestroyAffix(TAffix* affix);

	public:
		TAffixTable();
		~TAffixTable();

		void GetAffixs(TWordFrame &wf, wchar_t *str);
		int LoadFromFile(char *fileName);
		TTableInfo* TableInfo();
		TAffixInfo Check(wchar_t *str, int len);

		static int GetP(wchar_t l);

		static int GetT(wchar_t l);

		class TSearcher {
			friend class TAffixTable;
			private:
				int mCount;
				int *mode;
				int pos;
				int cat;
				wchar_t const*str;
				int len;
				TAffixBlock ***table;
				TAffixInfo *affInfo;
			public:
				TSearcher(){};
				//~TSearchIterator();
				TAffixInfo Search();
		};

		class TSearcherRoot {
			friend class TAffixTable;
			public:
				int mCount;
				int *mode;
				int pos;
				int cat;
				wchar_t const*str;
				int len;
				TAffixBlock ***table;

				TRootInfo *rootInfo;
			public:
				TSearcherRoot(){};
				//~TSearchIterator();
				TAffixInfo Search();
				TAffixInfo CheckRus();

		};

		void SetSearcher(TSearcher &iter, TAffixInfo &affInfo, wchar_t const* str, int len, int type) {
			iter.table = this->table;
			iter.pos=0;
			iter.cat=0;
			iter.mode = this->mode;
			iter.mCount = this->mCount-1;
			iter.len = len;
			iter.str = str;
			iter.affInfo = &affInfo;
			if(type!=1) iter.mCount = 15;
		};

		void SetSearcher(TSearcherRoot &iter, TRootInfo &rootInfo , wchar_t const* str, int len) {
			iter.table = this->table;
			iter.pos=0;
			iter.cat=0;
			iter.mode = this->mode;
			iter.mCount = this->mCount-1;
			iter.len = len;
			iter.str = str;
			iter.rootInfo = &rootInfo;
			if(rootInfo.type == 1) // ��� ����������
				iter.cat = 15;

		};

};




//---------------------------------------------------------------------------
#endif
