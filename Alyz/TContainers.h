//---------------------------------------------------------------------------

#ifndef TContainersH
#define TContainersH
#define defMaxAffs 40
class TWordFrame;

struct TAffixInfo {
	int cat, rcat;
	int var, rvar;
	int p, t;
	int len;
	wchar_t *str;
	//int pos;
	TAffixInfo() {
    	len = 0;
		cat = -1;
		var = -1;
		str = 0;
	};
	~TAffixInfo() {str=0;} ;
};

struct TRootInfo {
	int p;
	int t;
	int rus;
	int len;
	int type;
	int pos;
};

class TWordFrame {
	//public:
	friend class TRootBase;
	friend class TAffixTable;
	private:

		TAffixInfo affInfo[defMaxAffs];
		TRootInfo rootInfo;
		int len;
		int affCount;
		bool valid;
		//bool exept;
	public:

		TWordFrame(TRootInfo &rootSetting, int wlen) {
			rootInfo = rootSetting;
			affCount = 0;
			len = wlen;
			valid = 0;
			//exept = false;
		};
		TWordFrame(TWordFrame&, TAffixInfo&);
		~TWordFrame();

		int GetPos() {
			return rootInfo.pos;
        }

		void Add(TAffixInfo& ainf) {
			affInfo[affCount] = ainf;
			++affCount;
		}

		void SetValid() {
			valid = 1;
		}

		bool IsValid() {
        	return valid;
		}

		int Type() {
			return rootInfo.type;
        }

		void Reset() {
			affCount = 0;
			valid = 0;
        }

		void GetCats(int *v, int &len) {
			int j = 0;
			for(int i=affCount-1; i>=0; i--) {
				if(affInfo[i].len==0) continue;
				v[j] = affInfo[i].cat;
				++j;
			}
			len = j;
		};

		void GetRealCats(int *v, int &len) {
			int j = 0;
			for(int i=affCount-1; i>=0; i--) {
				if(affInfo[i].len==0) continue;
				v[j] = affInfo[i].rcat;
				++j;
			}
			len = j;
		};

		void GetVar(int *v) {
			int j = 0;
			for(int i=defMaxAffs-1; i>=0; i--) {
				if(affInfo[i].len==0) continue;
				v[j] = affInfo[i].var;
				++j;
			}
		};
		void GetRealVar(int *v) {
			int j = 0;
			for(int i=defMaxAffs-1; i>=0; i--) {
				if(affInfo[i].len==0) continue;
				v[j] = affInfo[i].rvar;
				++j;
			}
		};

		static void PrintFrame(TWordFrame &frame, wchar_t const* str);

};


//---------------------------------------------------------------------------
#endif
