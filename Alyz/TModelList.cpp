//---------------------------------------------------------------------------


#pragma hdrstop

#include "TModelList.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


TRegularModel* TModelList::CreateModel(int mlen, int *mod) {
	TRegularModel* model = new TRegularModel;

	model->len = mlen;
	model->model = new int[mlen];

	for(int i=0; i<mlen; i++) {
		model->model[i] = mod[i];
	}

	return model;
};

void TModelList::DestroyModel(TRegularModel *model) {
	if(model!=0)
		delete [] model->model;
};
/*
struct TUnregularModel {
	int mLen;
	int *model;
	int vCount;
	int **var;
};
*/
TUnregularModel* TModelList::CreateModel(int len, int *mod, int count, int **var) {
	TUnregularModel* model = new TUnregularModel;

	model->len = len;
	model->model = new int[len];
	for(int i=0; i<len; i++) {
		model->model[i] = mod[i];
	}

	model->count = count;
	model->var = new int*[count];
	for(int i=0; i<count; i++) {
		model->var[i] = new int[len];
		for(int j=0; j<len; j++) {
			model->var[i][j] = var[i][j];
        }
	}

	return model;
};

void TModelList::DestroyModel(TUnregularModel *model) {
	if(model!=0)
		delete [] model->model;

	for(int i=0; i<model->count; i++)
		delete [] model->var[i];

	delete [] model->var;
};

TModelList::TModelList() {
	nounReg = 0; nounRegCount = 0;

	nounUnreg = 0; nounUnregCount = 0;

	nounRegEx = 0; nounRegExCount = 0;

	nounUnregEx = 0; nounUnregExCount = 0;

	verbReg = 0; verbRegCount = 0;

	verbUnreg = 0; verbUnregCount = 0;
};

TModelList::~TModelList() {

	for(int i=0; i<nounRegCount; i++)
		DestroyModel(nounReg[i]);
	if(nounRegCount)delete [] nounReg;

	for(int i=0; i<nounUnregCount; i++)
		DestroyModel(nounUnreg[i]);
	if(nounUnregCount)delete [] nounUnreg;

	for(int i=0; i<nounRegExCount; i++)
		DestroyModel(nounRegEx[i]);
	if(nounRegExCount)delete [] nounRegEx;

	for(int i=0; i<nounUnregExCount; i++)
		DestroyModel(nounUnregEx[i]);
	if(nounUnregExCount)delete [] nounUnregEx;

	for(int i=0; i<verbRegCount; i++)
		DestroyModel(verbReg[i]);
	if(verbRegCount)delete [] verbReg;

	for(int i=0; i<verbUnregCount; i++)
		DestroyModel(verbUnreg[i]);
	if(verbUnregCount)delete [] verbUnreg;

};

void TModelList::LoadTableInfo(TTableInfo *info) {
	this->tableInfo.len = info->len;
	this->tableInfo.catSize = new int[info->len];

	for(int i=0; i<info->len; i++) {
		this->tableInfo.catSize[i] = info->catSize[i];
	}
	delete [] info->catSize;
	delete info;
};

bool TModelList::Load(char *filename, TRegularModel **modelList, int &count) {
	int c;
	ifstream ifs;
	ifs.open(filename, std::ios_base::in | std::ios_base::binary);
	if(ifs.is_open()==0) return false;
	ifs.read( (char*)&c, sizeof(int));
	count = c;
	modelList = new TRegularModel*[count];

	for(int i=0; i<count; i++) {
		int mLen;
		ifs.read( (char*)&mLen, sizeof(int));
		int buffer[10];
		ifs.read( (char*)&buffer, sizeof(int)*mLen);

		modelList[i] = CreateModel(mLen, buffer);
	}
	ifs.close();
	return true;
}

bool TModelList::Load(char *filename, TUnregularModel **modelList, int &count) {
	ifstream ifs;
	ifs.open(filename, std::ios_base::in | std::ios_base::binary);
	if(ifs.is_open()==0) return false;
	ifs.read( (char*)&count, sizeof(int));

	modelList = new TUnregularModel*[count];

	for(int i=0; i<count; i++) {
		int mLen;
		ifs.read( (char*)&mLen, sizeof(int));
		int buffer[10];
		ifs.read( (char*)&buffer, sizeof(int)*mLen);

		int vCount;
		ifs.read( (char*)&vCount, sizeof(int));

		int vbuf[10], **var = new int*[vCount];
		for(int j=0; j<vCount; j++) {
			ifs.read( (char*)&vbuf, sizeof(int)*mLen);
			var[j] = new int[mLen];
			for(int i=0; i<mLen; i++)
				var[j][i] = vbuf[i];
		}
		modelList[i] = CreateModel(mLen, buffer, vCount, var);

		for(int i=0; i<vCount; i++) delete [] var[i];
		delete [] var;
	}

	ifs.close();
	return true;
}
 /*
		TRegularModel **nounReg;
		int nCount;
		TUnregularModel **nounUnregular;
		int nurCount;
		TUnregularModel **nounExept;
		int nexCount;
		TRegularModel **verbRegular;
		int vCount;
		TUnregularModel **verbUnregular;
		int vurCount;
 */
 /*
int TModelList::LoadModels(char *pNounReg, char *pNounUnreg, char *pNounRegEx, char *pNounUnregEx, char *pVerbReg, char *pVerbUnreg) {
	Load(pNounReg, nounReg, nCount);
	Load(pNounUnreg, nounUnreg, nurCount);
	Load(pNounRegEx, nounRegEx, nounRegExCount);
	Load(pNounUnregEx, nounUnregEx, nounUnregExCount);

}
*/

int TModelList::LoadModels(char *fnNounReg, char *fnNounUnreg, char *fnNounRegEx, char *fnNounUnregEx,
		char *fnVerbReg, char *fnVerbUnreg, char *fnVerbRegEx, char *fnVerbUnregEx) {
	int count;

// ------------------- NOUN REGULAR --------------------------------------------
	ifstream ifs;
	if(fnNounReg==0) {
		this->nounRegCount = 0;
		this->nounReg = 0;
	} else {
		ifs.open(fnNounReg, std::ios_base::in | std::ios_base::binary);
		if(ifs.is_open()==0) return 0;
		ifs.read( (char*)&count, sizeof(int));

		this->nounRegCount = count;
		this->nounReg = new TRegularModel*[count];

		for(int i=0; i<count; i++) {
			int mLen;
			ifs.read( (char*)&mLen, sizeof(int));
			int buffer[10];
			ifs.read( (char*)&buffer, sizeof(int)*mLen);

			this->nounReg[i] = TModelList::CreateModel(mLen, buffer);
		}
	ifs.close();
	}
// ------------------- NOUN REGULAR EXEPTION -----------------------------------
	if(fnNounRegEx==0) {
		this->nounRegExCount = 0;
		this->nounRegEx = 0;
	} else {
		ifs.open(fnNounRegEx, std::ios_base::in | std::ios_base::binary);
		if(ifs.is_open()==0) return 0;
		ifs.read( (char*)&count, sizeof(int));

		this->nounRegExCount = count;
		this->nounRegEx = new TRegularModel*[count];

		for(int i=0; i<count; i++) {
			int mLen;
			ifs.read( (char*)&mLen, sizeof(int));
			int buffer[10];
			ifs.read( (char*)&buffer, sizeof(int)*mLen);

			this->nounRegEx[i] = TModelList::CreateModel(mLen, buffer);
		}
		ifs.close();
	}
// --------------------- NOUN UNREGULAR ----------------------------------------
	if(fnNounUnreg==0) {
		this->nounUnregCount = 0;
		this->nounUnreg = 0;
	} else {
		ifs.open(fnNounUnreg, std::ios_base::in | std::ios_base::binary);
		if(ifs.is_open()==0) return 0;
		ifs.read( (char*)&count, sizeof(int));

		this->nounUnregCount = count;
		this->nounUnreg = new TUnregularModel*[count];

		for(int i=0; i<count; i++) {
			int mLen;
			ifs.read( (char*)&mLen, sizeof(int));
			int buffer[10];
			ifs.read( (char*)&buffer, sizeof(int)*mLen);

			int vCount;
			ifs.read( (char*)&vCount, sizeof(int));

			int vbuf[10], **var = new int*[vCount];
			for(int j=0; j<vCount; j++) {
				ifs.read( (char*)&vbuf, sizeof(int)*mLen);
				var[j] = new int[mLen];
				for(int i=0; i<mLen; i++)
					var[j][i] = vbuf[i];
			}
			this->nounUnreg[i] = CreateModel(mLen, buffer, vCount, var);

			for(int i=0; i<vCount; i++) delete [] var[i];
			delete [] var;
		}

		ifs.close();
	}
// ------------------------ NOUN UNREGULAR EXEPTION-----------------------------
	if(fnNounUnregEx==0) {
		this->nounUnregExCount = 0;
		this->nounUnregEx = 0;
	} else {
		ifs.open(fnNounUnregEx, std::ios_base::in | std::ios_base::binary);
		if(ifs.is_open()==0) return 0;
		ifs.read( (char*)&count, sizeof(int));

		this->nounUnregExCount = count;
		this->nounUnregEx = new TUnregularModel*[count];

		for(int i=0; i<count; i++) {
			int mLen;
			ifs.read( (char*)&mLen, sizeof(int));
			int buffer[10];
			ifs.read( (char*)&buffer, sizeof(int)*mLen);

			int vCount;
			ifs.read( (char*)&vCount, sizeof(int));

			int vbuf[10], **var = new int*[vCount];
			for(int j=0; j<vCount; j++) {
				ifs.read( (char*)&vbuf, sizeof(int)*mLen);
				var[j] = new int[mLen];
				for(int i=0; i<mLen; i++)
					var[j][i] = vbuf[i];
			}
			this->nounUnregEx[i] = CreateModel(mLen, buffer, vCount, var);

			for(int i=0; i<vCount; i++) delete [] var[i];
			delete [] var;
		}

		ifs.close();
	}



// ---------------------- VERB REGULAR -----------------------------------------
	if(fnVerbReg==0) {
		this->verbRegCount = 0;
		this->verbReg = 0;
	} else {
		ifs.open(fnVerbReg, std::ios_base::in | std::ios_base::binary);
		if(ifs.is_open()==0) return 0;
		ifs.read( (char*)&count, sizeof(int));

		this->verbRegCount = count;
		this->verbReg = new TRegularModel*[count];

		for(int i=0; i<count; i++) {
			int mLen;
			ifs.read( (char*)&mLen, sizeof(int));
			int buffer[10];
			ifs.read( (char*)&buffer, sizeof(int)*mLen);

			this->verbReg[i] = TModelList::CreateModel(mLen, buffer);
		}
		ifs.close();
	}
// ------------------- VERB REGULAR EXEPTION -----------------------------------
	if(fnVerbRegEx==0) {
		this->verbRegExCount = 0;
		this->verbRegEx = 0;
	} else {
		ifs.open(fnVerbRegEx, std::ios_base::in | std::ios_base::binary);
		if(ifs.is_open()==0) return 0;
		ifs.read( (char*)&count, sizeof(int));

		this->verbRegExCount = count;
		this->verbRegEx = new TRegularModel*[count];

		for(int i=0; i<count; i++) {
			int mLen;
			ifs.read( (char*)&mLen, sizeof(int));
			int buffer[10];
			ifs.read( (char*)&buffer, sizeof(int)*mLen);

			this->verbRegEx[i] = TModelList::CreateModel(mLen, buffer);
		}
		ifs.close();
	}
// -------------------------VERB UNREGULAR -------------------------------------
	if(fnVerbUnreg==0) {
		this->verbUnregCount = 0;
		this->verbUnreg = 0;
	} else {
		ifs.open(fnVerbUnreg, std::ios_base::in | std::ios_base::binary);
		if(ifs.is_open()==0) return 0;
		ifs.read( (char*)&count, sizeof(int));

		this->verbUnregCount = count;
		this->verbUnreg = new TUnregularModel*[count];

		for(int i=0; i<count; i++) {
			int mLen;
			ifs.read( (char*)&mLen, sizeof(int));
			int buffer[10];
			ifs.read( (char*)&buffer, sizeof(int)*mLen);

			int vCount;
			ifs.read( (char*)&vCount, sizeof(int));

			int vbuf[10], **var = new int*[vCount];
			for(int j=0; j<vCount; j++) {
				ifs.read( (char*)&vbuf, sizeof(int)*mLen);
				var[j] = new int[mLen];
				for(int i=0; i<mLen; i++)
					var[j][i] = vbuf[i];
			}
			this->verbUnreg[i] = CreateModel(mLen, buffer, vCount, var);

			for(int i=0; i<vCount; i++) delete [] var[i];
			delete [] var;
		}
		ifs.close();
	}
// ------------------------ NOUN UNREGULAR EXEPTION-----------------------------
	if(fnVerbUnregEx==0) {
		this->verbUnregExCount = 0;
		this->verbUnregEx = 0;
	} else {
		ifs.open(fnVerbUnregEx, std::ios_base::in | std::ios_base::binary);
		if(ifs.is_open()==0) return 0;
		ifs.read( (char*)&count, sizeof(int));

		this->verbUnregExCount = count;
		this->verbUnregEx = new TUnregularModel*[count];

		for(int i=0; i<count; i++) {
			int mLen;
			ifs.read( (char*)&mLen, sizeof(int));
			int buffer[10];
			ifs.read( (char*)&buffer, sizeof(int)*mLen);

			int vCount;
			ifs.read( (char*)&vCount, sizeof(int));

			int vbuf[10], **var = new int*[vCount];
			for(int j=0; j<vCount; j++) {
				ifs.read( (char*)&vbuf, sizeof(int)*mLen);
				var[j] = new int[mLen];
				for(int i=0; i<mLen; i++)
					var[j][i] = vbuf[i];
			}
			this->verbUnregEx[i] = CreateModel(mLen, buffer, vCount, var);

			for(int i=0; i<vCount; i++) delete [] var[i];
			delete [] var;
		}
		ifs.close();
		}
	return 1;
};

TDetailedModel* TModelList::CreateDetailed(TRegularModel *mod) {
	TDetailedModel* detModel = new TDetailedModel;
	detModel->len = mod->len;
	detModel->model = new int[mod->len];
	for(int i=0; i<mod->len; i++)
		detModel->model[i] = mod->model[i];

	int totalVar = 1;
	for(int i=0; i<mod->len; i++)
		totalVar *= this->tableInfo.catSize[mod->model[i]];
	detModel->var = new int*[totalVar];
	detModel->count = totalVar;

	int *forward = new int[mod->len], *backward = new int[mod->len];

	forward[0] = 1;
	for(int i=1; i<mod->len; i++)
		forward[i] = forward[i-1] * this->tableInfo.catSize[mod->model[i-1]];

	backward[mod->len-1] = 1;
	for(int i = mod->len-2; i>=0; i--)
		backward[i] = backward[i+1] * this->tableInfo.catSize[mod->model[i+1]];

	for(int i=0; i<totalVar; i++) detModel->var[i] = new int[mod->len];

	for(int l=0; l<mod->len; l++) {
		int curr=0;
		for(int i=0; i<forward[l]; i++) {
			 for(int j=0; j<this->tableInfo.catSize[mod->model[l]]; j++){
				for(int k=0; k<backward[l]; k++) {
					detModel->var[curr][l] = j;
					curr++;
                }
            }
		}
	}


	delete [] backward;
	delete [] forward;

	return detModel;
};

TDetailedModel* TModelList::CreateDetailed(TUnregularModel *mod) {
	TDetailedModel* detModel = new TDetailedModel;

	int total=0;
	for(int c=0; c<mod->count; c++) {
		bool times = 0;
		for(int i=0; i<mod->len; i++) {
			if(mod->var[c][i]==-1) {
				if(!times)
					total += this->tableInfo.catSize[mod->model[i]];
				else
					total *= this->tableInfo.catSize[mod->model[i]];
				++times;
			}
		}
	}
	if (total<2) total = mod->count;

	detModel->var = new int*[total];
	for(int i=0; i<total; i++) detModel->var[i] = new int[mod->len];
	detModel->count = total;
	detModel->len = mod->len;
	detModel->model = new int[mod->len];
	for(int i=0; i<mod->len; i++)
		detModel->model[i] = mod->model[i];

	for(int c=0; c<mod->count; c++) {
		int totalVar = 1;
		for(int i=0; i<mod->len; i++)
			if(mod->var[c][i]==-1) totalVar *= this->tableInfo.catSize[mod->model[i]];


		int **var = new int*[totalVar];

		int *forward = new int[mod->len],
			*backward = new int[mod->len];

		forward[0] = 1;
		for(int i=1; i<mod->len; i++) {
			if(mod->var[c][i-1]==-1)
				forward[i] = forward[i-1] * this->tableInfo.catSize[mod->model[i-1]];
			else
				forward[i] = forward[i-1]*1;
		}

		backward[mod->len-1] = 1;
		for(int i = mod->len-2; i>=0; i--)
			if(mod->var[c][i+1]==-1)
				backward[i] = backward[i+1] * this->tableInfo.catSize[mod->model[i+1]];
			else
				backward[i] = backward[i+1] * 1;

		for(int i=0; i<totalVar; i++) var[i] = new int[mod->len];

		for(int l=0; l<mod->len; l++) {
			int curr=0;
			for(int i=0; i<forward[l]; i++) {
				 int catSize=1;
				 if(mod->var[c][l]==-1)
					catSize=this->tableInfo.catSize[mod->model[l]];
				 for(int j=0; j<catSize; j++){
					for(int k=0; k<backward[l]; k++) {
						var[curr][l] = j;
						curr++;
					}
				}
			}
		}

		for(int t=0; t<totalVar; t++) {
			for(int i=0; i<mod->len; i++) {
				if(mod->var[c][i]==-1)
					detModel->var[t+c*totalVar][i] = var[t][i];
				else
					detModel->var[t+c*totalVar][i] = mod->var[c][i];
			}
		}

		for(int i=0; i<totalVar; i++) delete [] var[i];
		delete [] var;
		delete [] backward;
		delete [] forward;
	}

	return detModel;
};


bool TModelList::CheckFrame(TWordFrame &frame) {
	int cats[defMaxAffs];
	int cLen;
	frame.GetRealCats(cats, cLen);
	TRegularModel **regular; int regCount;
	TUnregularModel **unregular; int unregCount;
	TRegularModel **regularExept; int regExeptCount;
	TUnregularModel **unregularExept; int unregExeptCount;

	if(frame.Type()!=1) {
		regular = nounReg;
		regCount = nounRegCount;
		unregular = nounUnreg;
		unregCount = nounUnregCount;

		regularExept = nounRegEx;
		regExeptCount = nounRegExCount;
		unregularExept = nounUnregEx;
		unregExeptCount = nounUnregExCount;
	} else {
		regular = verbReg;
		regCount = verbRegCount;
		unregular = verbUnreg;
		unregCount = verbUnregCount;

		regularExept = verbRegEx;
		regExeptCount = verbRegExCount;
		unregularExept = verbUnregEx;
		unregExeptCount = verbUnregExCount;
	}



	for(int i=0; i<regCount; i++) {
		if(regular[i]->len == cLen) {
			int cPos=0;
			for(cPos; cPos<regular[i]->len; cPos++)
				if( regular[i]->model[cPos] != cats[cPos]) break;
			if(cPos==cLen) {
				frame.SetValid();
				return true;
			}
		}
	}

	int vars[defMaxAffs];
	frame.GetRealVar(vars);

	for(int i=0; i<unregCount; i++) {
		if(unregular[i]->len == cLen) {
				UnicodeString str1, str2;
				for(int k=0; k<cLen; k++) {
					str1+=IntToStr(cats[k])+" ";
					str2+=IntToStr(unregular[i]->model[k])+" ";
				}
				//Form1->RichEdit4->Lines->Add(str1+" == "+str2+"  pos="+IntToStr(i));
			int cPos=0;

			for(cPos; cPos<unregular[i]->len; cPos++)
				if( unregular[i]->model[cPos] != cats[cPos]) break;
			if(cPos==cLen) {


				TDetailedModel *dm = CreateDetailed(unregular[i]);

				for(int i=0; i<dm->count; i++) {
					int vPos = 0;

					str1="\t"; str2="\t";
					for(int vPos=0; vPos<dm->len; vPos++) {
						str1+=IntToStr(vars[vPos])+" ";
						str2+=IntToStr(dm->var[i][vPos])+" ";
					}
					Form1->RichEdit4->Lines->Add(str1+" == "+str2+"  pos="+IntToStr(i));
					for(vPos; vPos<dm->len; vPos++)
						if(dm->var[i][vPos] != vars[vPos]) break;
					if(vPos==cLen) {
						frame.SetValid();
						delete dm;
						return true;
                    }
				}
				delete dm;
			}
		}
	}

	for(int i=0; i<regExeptCount; i++) {
		if(regularExept[i]->len == cLen) {    // !!!
			TExDetailedModel *edm = RepairModel(CreateDetailed(regularExept[i]));
			//Form1->RichEdit4->Lines->Add(IntToStr(regularExept[i]->len)+" == "+IntToStr(edm->dm->len));

			for(int i=0; i<edm->dm->count; i++) {


				UnicodeString str1, str2, str3, str4;
				for(int pos=0; pos<edm->dm->len; pos++) {
					str1+=IntToStr(cats[pos])+" ";
					str2+=IntToStr((*edm)(pos,i))+" ";
					str3+=IntToStr(vars[pos])+" ";
					str4+=IntToStr(edm->dm->var[i][pos])+" ";
				}
				/*Form1->RichEdit4->Lines->Add(str1+" == "+str2+"  pos="+IntToStr(i));
				Form1->RichEdit4->Lines->Add(str3+" == "+str4+"  pos="+IntToStr(i));
				Form1->RichEdit4->Lines->Add("");  */
				int pos;
				for(pos=0; pos<edm->dm->len; pos++) {
					if(cats[pos] != (*edm)(pos,i) || vars[pos] != edm->dm->var[i][pos])
						break;

					}
				if(pos==edm->dm->len) {
					delete edm;
					return true;
				}
			}
		}
	}

	for(int i=0; i<unregExeptCount; i++) {
		if(unregularExept[i]->len == cLen) {    // !!!
			TExDetailedModel *edm = RepairModel(CreateDetailed(unregularExept[i]));
			//Form1->RichEdit4->Lines->Add(IntToStr(unregularExept[i]->len)+" == "+IntToStr(edm->dm->len));

			for(int i=0; i<edm->dm->count; i++) {


				UnicodeString str1, str2, str3, str4;
				for(int pos=0; pos<edm->dm->len; pos++) {
					str1+=IntToStr(cats[pos])+" ";
					str2+=IntToStr((*edm)(pos,i))+" ";
					str3+=IntToStr(vars[pos])+" ";
					str4+=IntToStr(edm->dm->var[i][pos])+" ";
				}
				/*Form1->RichEdit4->Lines->Add(str1+" == "+str2+"  pos="+IntToStr(i));
				Form1->RichEdit4->Lines->Add(str3+" == "+str4+"  pos="+IntToStr(i));
				Form1->RichEdit4->Lines->Add("");   */
				int pos;
				for(pos=0; pos<edm->dm->len; pos++) {
					if(cats[pos] != (*edm)(pos,i) || vars[pos] != edm->dm->var[i][pos])
						break;

					}
				if(pos==edm->dm->len) {
					delete edm;
					return true;
				}
			}
		}
	}

	return false;
};

