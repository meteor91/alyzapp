﻿//---------------------------------------------------------------------------


#pragma hdrstop

#include "TAffixTable.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
void TAffixTable::DestroyAffix(TAffix *affix) {
	if(affix!=0)
		if(affix->str!=0)
			delete [] affix->str;
};

TAffixTable::TAffixTable() {
	table = 0;
	mCount = 0;
	mode = 0;
	#define m(i,j) mPossibleRes[(i)][(j)]
	m(0,0)=2; m(0,1)=3; m(0,2)=4; m(0,3)=5; m(0,4)=6; m(0,5)=7; m(0,6)=8; m(0,7)=11; m(0,8)=13; m(0,9)=14;
};

TAffixTable::~TAffixTable() {
	for(int i=0; i<this->mCount; i++) {
		for(int j=0; j<this->mode[i]; j++) {
			for(int k=0; k<4; k++) {
				for(int l=0; l<4; l++) {
					//DestroyAffix(this->table[i][j]->block[k][l]);
				}
			}
		}
		delete [] this->table[i];
	}
	delete [] this->table;

};

TAffix* TAffixTable::CreateAffix(wchar_t *pstr, int len) {
	TAffix *affix = new TAffix;
	affix->str = new wchar_t[len+1];
	wcscpy(affix->str, pstr);
	affix->str[len] = '\0';
	affix->len = len;
	affix->t = GetT(affix->str[len-1]);

	int l = len-1;
	affix->p = -1;
	//while( (affix->p = GetP(affix->str[l])) == -1)--l;
	while(l>=0) {
		affix->p = GetP(affix->str[l]);
		if(affix->p!=-1) break;
		l--;
	};
	return affix;
};

TAffixBlock* TAffixTable::CreateBlock() {
	TAffixBlock* table = new TAffixBlock;
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++) {
			table->block[i][j] = 0;
			table->block[i][j] = 0;
		}

	return table;
};

TTableInfo* TAffixTable::TableInfo() {
	TTableInfo* tf = new TTableInfo;
	tf->len = this->mCount;
	tf->catSize = new int[this->mCount];
	for(int i=0; i<this->mCount; i++) {
		tf->catSize[i] = this->mode[i];
	}

	return tf;
};

int TAffixTable::LoadFromFile(char *fileName) {
	ifstream ifs;
	ifs.open(fileName, std::ios_base::in | std::ios_base::binary);

	int count;
	ifs.read( (char*) &count, sizeof(int));
	if(!ifs.is_open()) return 0;

	this->mCount = count;
	this->mode = new int[this->mCount];
	this->table = new TAffixBlock**[this->mCount];

	int *mCount = new int[count];
	for(int i=0; i<count; i++) {
		int mC;
		ifs.read( (char*) &mC, sizeof(int));
		mCount[i] = mC;
		this->mode[i] = mC;
		this->table[i] = new TAffixBlock*[mC];
	}



	for(int i=0; i<count; i++) {
		for(int j=0; j<mCount[i]; j++) {
			this->table[i][j] = CreateBlock();
			for(int k=0; k<4; k++) {
				for(int n=0; n<4; n++) {
					short shstr[20];
					int len;
					ifs.read( (char*) &len, sizeof(int));


					ifs.read( (char*) &shstr, sizeof(short)*len);

					wchar_t *str = new wchar_t[len+1];
					for(int i=0; i<len; i++)
						str[i] = (wchar_t)shstr[i];

					str[len] = '\0';

					this->table[i][j]->block[k][n] = CreateAffix(str, len);


					delete [] str;
				}

			}

		}

	}
	return 1;
};

int TAffixTable::GetP(wchar_t l)
	   {

		int p=-1;
		  if((l==1241)|| // ә
			 (l==1077)||  //е
			 (l==1080)||  //и
			 (l==1102)||  //ю
			 (l==1199))   // ү
		  p=0;

		  if((l==1072)|| //а
			 (l==1091)||  //у
			 (l==1099)||  //ы
			 (l==1103)||  //я
			 (l==1101))   //э
		  p=1;

		  if(l==1257) // ө
		  p=2;

		  if(l==1086) //о
		  p=3;
	   return p;
	   }
 //////////////////////////////////////////////////////////
int TAffixTable::GetT(wchar_t l){
		int t=-1;
		  if((l==1241)|| // ә
			(l==1077)|| //е
			(l==1101)|| // э
			(l==1072)|| // а
			(l==1099)|| // ы
			(l==1257)||  //ө
			(l==1086)||  // о
			(l==1103))  //я
		  t=0;

		   if((l==1083)||//л
			(l==1084)||  //м
			(l==1085)||  //н
			(l==1187)||  // ң
			(l==1078)||  //ж
			(l==1079))   //з
		  t=1;

		   if((l==1080)|| //и
			(l==1102)||   //ю
			(l==1091)||   //у
			(l==1177)||  //ҙ
			(l==1088)||  //р
			(l==1081)||  //й
			(l==1199))   //ү
		  t=2;

		  if((l==1073)|| // б
			(l==1074)||  //  в
			(l==1075)||  //  г
			(l==1171)||  //  ғ
			(l==1076)||  // д
			(l==1087)||  // п
			(l==1095)||  // ч
			(l==1090)||  // т
			(l==1185)||  //ҡ
			(l==1082)||  // к
			(l==1092)||  // ф
			(l==1089)||  // с
			(l==1195)|| //ҫ
			(l==1211)||  //һ
			(l==1096)|| //  ш
			(l==1093)|| //  х
			(l==1094)||     //  ц
			(l==1097))      //  щ
		  t=3;

		  if( l==1066 || l==1098 || l==1068 || l==1100 ) t=-2;
	   return t;
}


TAffixInfo TAffixTable::TSearcherRoot::CheckRus() {
	TAffixInfo lAffInfo;
	if(rootInfo->rus)
		if(len>1)
			if((str[0]==1082) && (str[1]==1072)) {
				lAffInfo.cat = 1;
				lAffInfo.var = 1;
				lAffInfo.len = 2;
				lAffInfo.t = 0;
				lAffInfo.p = 1;
				pos++;
				return lAffInfo;
			}
	return lAffInfo;
}
TAffixInfo TAffixTable::TSearcherRoot:: Search() {
	//  len - остаточная длина
	TAffixInfo lAffInfo; lAffInfo.len=0;
	for(cat; cat<mCount-1; cat++) {

		for(pos; pos<mode[cat]; pos++) {
			int lLen;
			for(lLen=0; lLen<len && lLen<table[cat][pos]->block[ rootInfo->t ][ rootInfo->p ]->len; lLen++)
				if(table[cat][pos]->block[ rootInfo->t ][ rootInfo->p ]->str[lLen]!=str[lLen])break;

			if(lLen == table[cat][pos]->block[ rootInfo->t ][ rootInfo->p ]->len) {
				lAffInfo.cat = cat; lAffInfo.rcat = cat;
				lAffInfo.var = pos; lAffInfo.rvar = pos;
				lAffInfo.p = table[cat][pos]->block[ rootInfo->t ][ rootInfo->p ]->p;
				if(lAffInfo.p<0)
					lAffInfo.p = rootInfo->p;
				lAffInfo.t = table[cat][pos]->block[ rootInfo->t ][ rootInfo->p ]->t;
				lAffInfo.len = lLen;
				lAffInfo.str = table[cat][pos]->block[ rootInfo->t ][ rootInfo->p ]->str;
				pos++;
				return lAffInfo;
			}
		}
		pos = 0;
	}
	return lAffInfo;
};

TAffixInfo TAffixTable::TSearcher::Search() {
	//Form1->RichEdit3->Lines->Add("\tserach going...");
	//Form1->RichEdit3->Lines->Add("\t\tgot:"+Unicod);
	TAffixInfo lAffInfo; lAffInfo.len=0;
	for(cat; cat<mCount; cat++) {

		for(pos; pos<mode[cat]; pos++) {
			//Form1->RichEdit2->Lines->Add("pos: ["+IntToStr(pos)+"/"+IntToStr(mode[cat])+"  |  "+IntToStr(cat)+"/"+IntToStr(mCount)+"]");
			int lLen;
			/*Form1->RichEdit3->Lines->Add("\t["+IntToStr(cat)+"/"+IntToStr(pos)+"]compare: " + UnicodeString(table[cat][pos]->block[ affInfo->t ][ affInfo->p ]->str,
																	   table[cat][pos]->block[ affInfo->t ][ affInfo->p ]->len)
										+" == "+UnicodeString(str,len));                      */
			for(lLen=0; lLen<len && lLen<table[cat][pos]->block[ affInfo->t ][ affInfo->p ]->len; lLen++)
				if(table[cat][pos]->block[ affInfo->t ][ affInfo->p ]->str[lLen]!=str[lLen])break;

			if(lLen == table[cat][pos]->block[ affInfo->t ][ affInfo->p ]->len) {
				lAffInfo.cat = cat; lAffInfo.rcat = cat;
				lAffInfo.var = pos; lAffInfo.rvar = pos;
				lAffInfo.p = table[cat][pos]->block[ affInfo->t ][ affInfo->p ]->p;
				if(lAffInfo.p<0) {
					lAffInfo.p = affInfo->p;
					//Form1->RichEdit1->Lines->Add("Take last p="+IntToStr(lAffInfo.p));
				}
				lAffInfo.t = table[cat][pos]->block[ affInfo->t ][ affInfo->p ]->t;
				lAffInfo.len = lLen;
				lAffInfo.str = table[cat][pos]->block[ affInfo->t ][ affInfo->p ]->str;
				pos++;
				//Form1->RichEdit2->Lines->Add("finded at: ["+IntToStr(cat)+" | "+IntToStr(pos)+"]");
				return lAffInfo;
			}

		}
		pos = 0;
		/*Form1->RichEdit2->Lines->Add("compare LOL"+IntToStr(pos)+" == "+IntToStr(mode[cat]-1));
		if(pos==(mode[cat]-1)) pos = 0;   */
	}

	//if(mCount<16)
	 if((affInfo->cat==3)||(affInfo->cat==7)) {
		for(pos; pos<mode[30]; pos++) {
			int lLen;

			for(lLen=0; lLen<len && lLen<table[30][pos]->block[ affInfo->t ][ affInfo->p ]->len; lLen++)
				if(table[30][pos]->block[ affInfo->t ][ affInfo->p ]->str[lLen]!=str[lLen])break;

			if(lLen == table[30][pos]->block[ affInfo->t ][ affInfo->p ]->len) {
				lAffInfo.cat = 30; lAffInfo.rcat = 30;
				lAffInfo.var = pos; lAffInfo.rvar = pos;
				lAffInfo.p = table[30][pos]->block[ affInfo->t ][ affInfo->p ]->p;
				if(lAffInfo.p<0) {
					lAffInfo.p = affInfo->p;
					//Form1->RichEdit1->Lines->Add("Take last p="+IntToStr(lAffInfo.p));
				}
				lAffInfo.t = table[30][pos]->block[ affInfo->t ][ affInfo->p ]->t;
				lAffInfo.len = lLen;

				if(affInfo->cat==3) {
					lAffInfo.cat = 1;
					if((affInfo->var==0 || affInfo->var==1) && (pos==0)) lAffInfo.var = 1;
					else if((affInfo->var==2 ) && (pos==3)) { lAffInfo.var = 4; }
					else if(affInfo->var==2 && pos==4) {lAffInfo.var = 0; lAffInfo.cat = 10;}
				} else if(affInfo->cat==7) {
					lAffInfo.cat = 1;
					if((affInfo->var==0) && (pos==3)) lAffInfo.var = 4;
				}

				lAffInfo.t = table[30][pos]->block[ affInfo->t ][ affInfo->p ]->t;
				++pos;

				return lAffInfo;

			}
		}
	 }

	return lAffInfo;
}

void TAffixTable::GetAffixs(TWordFrame &wf, wchar_t *str) {

	if(wf.affCount!=0) {
		wcscat(str, table[wf.affInfo[wf.affCount-1].cat][wf.affInfo[wf.affCount-1].var]->block[wf.rootInfo.t][wf.rootInfo.p]->str);
		wcscat(str, L" ");

		for(int i=wf.affCount-2; i>=0; i--) {
			wcscat(str, table[wf.affInfo[i].rcat][wf.affInfo[i].rvar]->block[wf.affInfo[i+1].t][wf.affInfo[i+1].p]->str);
			wcscat(str, L" ");
		}
	}
};
