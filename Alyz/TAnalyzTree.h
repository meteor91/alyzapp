//---------------------------------------------------------------------------

#ifndef TAnalyzTreeH
#define TAnalyzTreeH
#include "TContainers.h"

#define defMaxActual 100
#define defMaxBraches 5

struct TreeNode {
	TAffixInfo affInf;
	TreeNode *prev;
	TreeNode *next[defMaxBraches];
	int count;
	int pos;
	static tcount;

	TreeNode(TAffixInfo &paffInf) {
		for(int i=0; i<defMaxBraches; i++) next[i] = 0;
		++tcount;
		affInf = paffInf;
		count = 0;
		prev = 0;
	}
	~TreeNode() {--tcount;}
};

class ActualBranchList {
	private:
		TreeNode *actualNode[defMaxActual];
		TreeNode *finishedNode[defMaxActual];
		int current;
		int fCurrent;
		int fCount;
	public:

		ActualBranchList() {
			current = 0;
			fCurrent = 0;
			fCount = 0;
			for(int i=0; i<defMaxActual; i++) {
				actualNode[i] = 0;
				finishedNode[i] = 0;
			}
		};

		~ActualBranchList() {
			current = 0;
			fCurrent = 0;
			fCount = 0;
			for(int i=0; i<defMaxActual; i++) {
				actualNode[i] = 0;
				finishedNode[i] = 0;
			}
		};

		void Clear() {
			current = 0;
			fCurrent = 0;
			fCount = 0;
			for(int i=0; i<defMaxActual; i++) {
				actualNode[i] = 0;
				finishedNode[i] = 0;
			}
        }

		void Add(TreeNode *node) {
			for(int i=0; i<defMaxActual; i++)
				if(actualNode[i] == 0) {
					actualNode[i]=node;
					break;
				}
		};

		void Finish(TreeNode *node) {
			if(fCount==defMaxActual) return;
			finishedNode[fCount] = node;
			++fCount;
		};

		int GetFinishedCount() {
            return fCount;
        }

		int Curr() {
			return current;
        }

		TreeNode* Get() {
			return actualNode[current];
		};

		void UpdateCurrent(TreeNode *node) {
			actualNode[current] = node;
		};




		void SetInactual() {
			actualNode[current] = 0;
		};

		bool Start() {
			current = -1;
            fCurrent = 0;
			if(actualNode[0]==0) return false;
			else return true;

		};


		bool Next() {
			++current;
			for(current; current<defMaxActual; current++)
				if(actualNode[current]!=0) {
					return true;
				}
			return false;
		};


		int GetCount() {
			int count=0;
			for(int i=0; i<defMaxActual; i++)
				if(actualNode[i]!=0) ++count;

			return count;
        }

		bool GetTest(TAffixInfo *v) {
			TreeNode *buf = finishedNode[fCurrent];
			++fCurrent;
			if (buf==0) return false;
			int i=5;
			do {
				if(i==-1)break;
				v[i] = buf->affInf;
				//Form1->RichEdit1->Lines->Add(IntToStr)
				i--;
				buf = buf->prev;
			} while(buf!=0);
			return true;
		}

		bool GetTest(TWordFrame &v) {
			TreeNode *buf = finishedNode[fCurrent];
			++fCurrent;
			if (buf==0) return false;
			do {
				v.Add(buf->affInf);
				buf = buf->prev;
			} while(buf!=0);
			return true;
		}


};




class TAnalyzeTree {
	public:

		TRootInfo rootInfo;
		TreeNode *root[defMaxBraches];
		int len;
		int mRootLen;
		int currNode;
		ActualBranchList aBrList;
		void destructor(TreeNode *node);

		TreeNode *current;
		TreeNode *lastAdded;
	public:

		TAnalyzeTree(int l, int rootLen) {
			for(int i=0; i<defMaxBraches; i++) root[i] = 0;
			currNode = 0;
			len = l;
			mRootLen = rootLen;
		};

		~TAnalyzeTree() {
			aBrList.Clear();
			destructor(root[0]);
			destructor(root[1]);
			destructor(root[2]);

		};
		void Clear() {
			aBrList.Clear();
			destructor(root[0]);
			destructor(root[1]);
			destructor(root[2]);


		};


		bool AddFirst(TAffixInfo &affInf) {
			if(currNode>2) return false;

			TreeNode *node = new TreeNode(affInf);
			root[currNode] = node;
			root[currNode]->pos = mRootLen + affInf.len;
			lastAdded = node;

			if(node->pos >= len) {
				aBrList.Finish(node);
			} else
				aBrList.Add(node);
				++currNode;

			return true;
		};


		bool Start() {
			return aBrList.Start();
		};

		bool SetLastOne() {
			if(aBrList.Next()) {
				current = aBrList.Get();
				aBrList.SetInactual();

				return true;
			} else
				return false;
		};


		void AddToCurrent(TAffixInfo& affInf) {
			TreeNode *node = new TreeNode(affInf);
			node->pos = current->pos + node->affInf.len;
			current->next[current->count]=node;

			node->prev = current;
			lastAdded = node;
			if(node->pos >= len)
				aBrList.Finish(node);
			else
				aBrList.Add(node);
			++current->count;
		};

		TAffixInfo Current() {
			return current->affInf;
		}

		int CurrentLen() {
			return current->pos;
		}

		void GetLens(int *lens) {   // delete function later;
			TreeNode *buf = current;
			int i=5;
			do {
				lens[i] = buf->affInf.len;
				//Form1->RichEdit1->Lines->Add(IntToStr)
				i--;
				buf = buf->prev;
			} while(buf!=0);

			for(i; i>=0; i--) lens[i] = 0;

		}
};

void TAnalyzeTree::destructor(TreeNode *node) {
	if(node!=0) {
		for(int i=0; i<defMaxBraches; i++) destructor(node->next[i]);

		delete node;
		node = 0;
	}
}

//---------------------------------------------------------------------------
#endif
