//---------------------------------------------------------------------------

#ifndef TRootBaseH
#define TRootBaseH

#include<fstream>
#include<vector.h>
#include<wchar.h>

#include "TContainers.h"
//#include"TAffixTable.h"

class TRoot;



class TRootBase {
	public:
		TRoot **mRoot;
		int mCount;
	public:
		TRootBase() {
			mRoot = 0;
			mCount = 0;
		}
		~TRootBase();

		int LoadFormFile(char *filename);
		int Size() { return mCount;};
		static TRoot* CreateRoot(int ptype, wchar_t* pstr, wchar_t *pcstr, int pcp, int prus);
		static void DestroyRoot(TRoot* root);

		void SearchRoot(wchar_t const*str, int len, vector<TRootInfo>& rs);

		void GetRoot(TWordFrame &wf, wchar_t *str);

		int Len(int i);

};

class TRoot {
	friend class TRootBase;

	public:
		int mType;
		wchar_t **mStr;
		int mLen[2];//, clen;
		int mRus;
		int mCp;

		TRoot(){};
};




//---------------------------------------------------------------------------
#endif

