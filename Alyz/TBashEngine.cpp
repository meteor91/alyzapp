//---------------------------------------------------------------------------


#pragma hdrstop

#include "TBashEngine.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
TAffixTable TBashAnalyzer::AffixTable;
TRootBase TBashAnalyzer::RootBase;
TModelList TBashAnalyzer::ModelList;



TWordFrame::~TWordFrame() {

};



void TBashAnalyzer::Init() {
	AffixTable.LoadFromFile("res//affix.besh");
	RootBase.LoadFormFile("res//roots.besh");
	ModelList.LoadModels("res2//nrm.besh",
						 "res2//nurm.besh",
						 "res2//nerm.besh",
						 "res2//neurm.besh",
						 "res2//vrm.besh",
						 "res2//vurm.besh",
						 0,
						 "res2//veurm.besh");
	ModelList.LoadTableInfo(AffixTable.TableInfo());

};

void TBashAnalyzer::AnalyzeWord(wchar_t const* STR, vector<TWordFrame> &vWordFrame) {

	const int WORD_LEN = wcslen(STR);
	vector<TRootInfo> roots;
	RootBase.SearchRoot(STR, WORD_LEN, roots);

	//Form1->RichEdit3->Lines->Add("find roots count: "+IntToStr((int)roots.size()));

	for(int r=0; r<roots.size(); r++) {
		//Form1->RichEdit3->Lines->Add("work with: "+UnicodeString(STR, roots[r].len)+" type="+IntToStr(roots[r].type)+" pos="+IntToStr(roots[r].pos));
		if(roots[r].len==WORD_LEN) {
			TWordFrame frame(roots[r], WORD_LEN);
			frame.SetValid();
			vWordFrame.push_back(frame);
			continue;
        }
		TAffixTable::TSearcherRoot rootSearchIter;
		AffixTable.SetSearcher(rootSearchIter,
							   roots[r],
							   STR + roots[r].len,
							   WORD_LEN - roots[r].len);


		TAffixInfo searchResult;
		searchResult.len=0;
		TAnalyzeTree analyzeTree(WORD_LEN, roots[r].len);
		//Form1->RichEdit3->Lines->Add(UnicodeString(STR, roots[r].len)+" mCount="+IntToStr(rootSearchIter.mCount));

		searchResult = rootSearchIter.CheckRus();
		if(searchResult.len!=0) {
			analyzeTree.AddFirst(searchResult);
		}
		else
		do {
			searchResult = rootSearchIter.Search();
			if(searchResult.len!=0) {
				//Form1->RichEdit3->Lines->Add(UnicodeString(STR + roots[r].len, searchResult.len)+" t="+IntToStr(searchResult.t)+" p="+IntToStr(searchResult.p));
				//Form1->RichEdit3->Lines->Add(" root= "+IntToStr(roots[r].len) + " aff= "+IntToStr(searchResult.len));
				analyzeTree.AddFirst(searchResult);

				//Form1->RichEdit3->Lines->Add(" NEW POS - "+IntToStr(analyzeTree.lastAdded->pos));

			}
		} while(searchResult.len!=0);

		//Form1->RichEdit3->Lines->Add("");


		while(analyzeTree.Start()) {
			while(analyzeTree.SetLastOne()) {
				TAffixInfo affInfo = analyzeTree.Current();
				//Form1->RichEdit3->Lines->Add("Last one setted");
				//Form1->RichEdit3->Lines->Add("t="+IntToStr(affInfo.t)+" p="+IntToStr(affInfo.p));
				int currPos = analyzeTree.CurrentLen();
				//Form1->RichEdit3->Lines->Add("current pos = "+IntToStr(currPos)+"  "+UnicodeString(STR + currPos, WORD_LEN - currPos));
				TAffixTable::TSearcher searchIter;
				//int SetIterator(TSearchIterator &iter, TAffixInfo &affInfo, wchar_t const* str, int len)

				AffixTable.SetSearcher(searchIter,
									   affInfo,
									   STR + currPos,
									   WORD_LEN - currPos,
									   roots[r].type);

				do {
					searchResult = searchIter.Search();
					if(searchResult.len!=0) {
						//Form1->RichEdit3->Lines->Add(UnicodeString(STR + currPos, searchResult.len));
						/*int lens[6];
						// ------------- view result -----------------------
							analyzeTree.GetLens(lens);
							UnicodeString strLens=UnicodeString(STR, roots[r].len);
							int currPos = roots[r].len;
							for(int i=0;i<6;i++) {
								if(lens[i]==0)continue;
								strLens+=" "+UnicodeString(STR+currPos, lens[i]);
								currPos+=lens[i];
							}
							strLens+=" + "+UnicodeString(STR+currPos, searchResult.len)+"["+IntToStr(searchResult.len)+"-"+IntToStr(searchResult.t)+"|"+IntToStr(searchResult.p)+"]";;
							Form1->RichEdit3->Lines->Add(strLens+"    "+UnicodeString(STR+currPos+searchResult.len, WORD_LEN-currPos-searchResult.len));
						// ------------------------------------------------
						*/analyzeTree.AddToCurrent(searchResult);
					}
				} while(searchResult.len!=0);
			}
		};

//  -----------------------  Формирование окончательного результата ------------
		TWordFrame frame(roots[r], WORD_LEN);
		analyzeTree.aBrList.Start();
		analyzeTree.aBrList.GetCount();

		while(analyzeTree.aBrList.GetTest(frame)) {
			if(ModelList.CheckFrame(frame)) {
					frame.SetValid();
					vWordFrame.push_back(frame);
			}

			// -----------------------------------------------------------------
			 /*	wchar_t wfstr[20];
				Form1->BashEngine.RootBase.GetRoot(frame, wfstr);
				int cats[10], vars[10];
				int count;
				frame.GetCats(cats, count);
				frame.GetVar(vars);
				UnicodeString strCats, strVars;
				if(count!=0) {
					strCats = " Модель: [ ", strVars = "[ ";
					for(int i=0; i<count; i++) {
						strCats += IntToStr(cats[i])+" ";
						strVars += IntToStr(vars[i])+" ";
					}
					strCats += " ]";
					strVars += " ]";
				}
				Form1->BashEngine.AffixTable.GetAffixs(frame, wfstr);
				Form1->RichEdit3->Lines->Add("Итого:");
				Form1->RichEdit3->Lines->Add(wfstr);
				Form1->RichEdit3->Lines->Add("Корень: " + IntToStr(frame.GetPos()+1) +
									  strCats + " " + strVars);
				Form1->RichEdit3->Lines->Add("");
			*/// -----------------------------------------------------------------
			frame.Reset();
		}
	}
};




