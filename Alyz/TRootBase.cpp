//---------------------------------------------------------------------------


#pragma hdrstop

#include "TRootBase.h"
#include "TAffixTable.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)


TRootBase::~TRootBase() {
	for(int i = 0 ; i < mCount ; i++ ) {
		TRootBase::DestroyRoot(mRoot[i]);
	}

	delete [] mRoot;
};

int TRootBase::LoadFormFile(char *fileName) {

	ifstream ifs;
	ifs.open(fileName, std::ios_base::in | std::ios_base::binary);

	if(ifs.is_open()) {
		int count;
		ifs.read( (char*) &count, sizeof(int));
		this->mCount = count;
		this->mRoot = new TRoot*[count];

		for(int i=0 ; i<count ; i++) {

			int type;
			wchar_t **str;
			int len[2];
			int rus;
			int cp;

			ifs.read( (char*) &type, sizeof(int));
			ifs.read( (char*) &len[0], sizeof(int));
			ifs.read( (char*) &len[1], sizeof(int));


			str = new wchar_t*[2];
			str[0] = new wchar_t[len[0]+1];
			short strbuf[20];
			ifs.read( (char*) &strbuf, sizeof(short)*len[0]);
			for(int i=0; i<len[0]; i++)
				str[0][i] = (wchar_t)strbuf[i];
			str[0][len[0]] = '\0';


			if(len[1]!=0) {
				str[1] = new wchar_t[len[1]+1];
				ifs.read( (char*) &strbuf, sizeof(wchar_t)*len[1]);
				for(int i=0; i<len[1]; i++)
					str[1][i] = (wchar_t)strbuf[i];
				str[1][len[1]] = '\0';
			} else str[1] = 0;

			ifs.read( (char*) &rus, sizeof(int));
			ifs.read( (char*) &cp, sizeof(int));

			this->mRoot[i] = TRootBase::CreateRoot(type, str[0], str[1], cp, rus);

			delete [] str[0];
			if(str[1]!=0) delete [] str[1];
			delete [] str;
		}

		ifs.close();

		return 1;
	} else  return 0;
};

TRoot* TRootBase::CreateRoot(int ptype, wchar_t* pstr, wchar_t *pcstr, int pcp, int prus) {
	TRoot* root = new TRoot;
	root->mType = ptype;
	root->mRus = prus;
	root->mCp = pcp;

	if(pcstr!=0)root->mLen[1] = wcslen(pcstr); else root->mLen[1] = 0;
	root->mLen[0] = wcslen(pstr);

	root->mStr = new wchar_t*[2];

	root->mStr[0] = new wchar_t[root->mLen[0]+1];
	wcscpy(root->mStr[0], pstr);
	root->mStr[0][root->mLen[0]] = '\0';

	if(root->mLen[1]!=0){
		root->mStr[1] = new wchar_t[root->mLen[1]+1];
		wcscpy(root->mStr[1], pcstr);
		root->mStr[1][root->mLen[1]] = '\0';
	} else
		root->mStr[1] = 0;


	return root;
}

void TRootBase::DestroyRoot(TRoot *root) {
	delete [] root->mStr[0];

	if(root->mLen[1]!=0)
		delete [] root->mStr[1];

	delete [] root->mStr;
}

int TRootBase::Len(int i) {
	return this->mRoot[i]->mLen[0];
};


void  TRootBase::SearchRoot(wchar_t const*STR, int LEN, vector<TRootInfo>& RS) {
	TRootInfo setting;
	RS.clear();

	for(int i=0; i<this->mCount; i++) {
		if (this->mRoot[i]->mLen[0] > LEN) continue;
		if (this->mRoot[i]->mLen[0] == 1) continue;

		int var = 0?1 : this->mRoot[i]->mLen[1]!=0;
		if( (LEN==this->mRoot[i]->mLen[0]) && (STR[LEN-1] == this->mRoot[i]->mStr[0][LEN-1]) ) var = 0;

		int l;
		for(l=0;l<this->mRoot[i]->mLen[0];l++)
			if(STR[l] != this->mRoot[i]->mStr[var][l]) break;

		if(l==this->mRoot[i]->mLen[var]) {
			int j = this->mRoot[i]->mLen[var]-1;
			setting.t = TAffixTable::GetT(this->mRoot[i]->mStr[var][j]);

			if(this->mRoot[i]->mCp == 1) setting.p = 1;
			else if(this->mRoot[i]->mCp == 2) setting.p = 0;
			else while( (setting.p = TAffixTable::GetP(this->mRoot[i]->mStr[var][j])) == -1)--j;

			setting.type = this->mRoot[i]->mType;
			setting.rus = this->mRoot[i]->mRus;
			setting.len = this->mRoot[i]->mLen[var];
			setting.pos = i;

			if(this->mRoot[i]->mStr[var][l-1] == 1185 || this->mRoot[i]->mStr[var][l-1] == 1087)
				if(LEN>this->mRoot[i]->mLen[0]) {
					wchar_t ch = STR[l];
					if((ch == 1072)||(ch == 1199)||(ch == 1080)||(ch == 1099)||(ch == 1241)||(ch == 1086)||(ch == 1102)||
							(ch == 1077)||(ch == 1105)||(ch == 1257)||(ch == 1091)||(ch == 1103)  ) continue;
				}

			RS.push_back(setting);

		} else if ( (LEN>this->mRoot[i]->mLen[0]) && (var==0) && (l==(this->mRoot[i]->mLen[0]-1)) ) {
			wchar_t ch = STR[l+1];
			if( (this->mRoot[i]->mStr[var][l] == 1087 && STR[l] == 1073) || (this->mRoot[i]->mStr[var][l] == 1185) && STR[l] == 1171)   // � k
			if((ch == 1072)||(ch == 1199)||(ch == 1080)||(ch == 1099)||(ch == 1241)||(ch == 1086)||(ch == 1102)||
							(ch == 1077)||(ch == 1105)||(ch == 1257)||(ch == 1091)||(ch == 1103)  ) {

				int j = this->mRoot[i]->mLen[var]-1;
				setting.t = TAffixTable::GetT(this->mRoot[i]->mStr[var][j]);

				if(this->mRoot[i]->mCp == 1) setting.p = 2;
				else if(this->mRoot[i]->mCp == 2) setting.p = 1;
				else while( (setting.p = TAffixTable::GetP(this->mRoot[i]->mStr[var][j])) == -1)--j;

				setting.rus = this->mRoot[i]->mRus;
				setting.type = this->mRoot[i]->mType;
				setting.len = this->mRoot[i]->mLen[0];
				setting.pos = i;
				RS.push_back(setting);
			}
		}

	}
};

void TRootBase::GetRoot(TWordFrame &wf, wchar_t *str) {
	wcscpy(str, mRoot[wf.rootInfo.pos]->mStr[0]);
	wcscat(str, L" ");
};

